class AddAttachmentAttachedImageToPosts < ActiveRecord::Migration
  def self.up
    change_table :posts do |t|
      t.attachment :attached_image
    end
  end

  def self.down
    remove_attachment :posts, :attached_image
  end
end
