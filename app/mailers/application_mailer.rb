class ApplicationMailer < ActionMailer::Base
  default from: 'from@example.com'
  layout 'mailer'

  def new_user(user)
    @user = user
    template_name = 'new-user'
    template_content = []
    message = {
      to: [{ email: user.email }],
      subject: 'New User',
      merge_vars: [
        { rcpt: user.email,
          vars: [] }
      ]
    }
  end
end
