class CommentMailer < ActionMailer::Base
  def comment_created(current_user, _post_user, body)
    @current_user = current_user
    @body = body
    mail(to: current_user.email,
         from: 'marysaphpir@mydomain.com',
         subject: 'Comment Created')
  end
end
