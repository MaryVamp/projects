class PostMailer < ActionMailer::Base
  def post_created(user)
    @user = user
    mail(to: user.email,
         from: 'marysaphpir@mydomain.com',
         subject: 'Post Created',
         body: "You've created the post")
  end
end
