class AdminMailer < ApplicationMailer
  # default from: "no-reply@gmail.com"
  # default to: "marysamphpir@gmail.com"

  def mandrill_client
    @mandrill_client ||= Mandrill::API.new MANDRILL_API_KEY
  end

  # def new_user(user)
  #   @user = user
  #   mail(subject: "New user #{user.email} was registreted on your website")
  # end

  def new_post(post)
    template_name = 'new-post'
    template_content = []
    message = {
      to: [{ email: user.email }],
      subject: "New Post: #{post.title}",
      merge_vars: [
        { rcpt: user.email,
          vars: [
            { name: 'POST_NAME', content: post.title }
          ] }
      ]
    }

    def new_user(user)
      @user = user
      template_name = 'new-user'
      template_content = []
      message = {
        to: [{ email: user.email }],
        subject: 'New User',
        merge_vars: [
          { rcpt: user.email,
            vars: [] }
        ]
      }
    end

    mandrill_client.message.send_teplate template_name, template_content, message
  end

  def new_comment(comment)
    @user = user
    template_name = 'new-comment'
    template_content = []
    message = {
      to: [{ email: user.email }],
      subject: 'New Comment',
      merge_vars: [
        { rcpt: user.email,
          vars: [
            { name: 'COMMENT_NAME', content: comment.name }
          ] }
      ]
    }

    mandrill_client.message.send_teplate template_name, template_content, message
end
end
