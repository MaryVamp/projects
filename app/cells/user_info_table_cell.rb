class UserInfoTableCell < Cell::ViewModel
  def show
    current_user = User.find(params[:user_id])
    @posts = current_user.posts
    @micropost = current_user.messages
    render
  end

  private
end
