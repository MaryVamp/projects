class UsersController < ApplicationController
  before_action :admin_user, only: :destroy

  def index
    @users = User.paginate(page: params[:page], per_page: 5)
  end

  def edit
  end

  def show
    @user = User.find(params[:id])
  end

  def new
    @user = User.new
  end

  def create
    @user = User.new(params[:id])
    if @user.save
      AdminMailer.new_user(self).deliver_now
      flash[:notice] = 'Success'
      redirect_to @user
    else
      render 'new'
    end

    def update
      @user = User.find(params[:id])
      if @user.update_attributes(user_params)
        flash[:success] = 'Profile updated'
        redirect_to @user
      else
        render 'edit'
      end
    end
  end

  def destroy
    User.find(params[:id]).destroy
    flash[:success] = 'User deleted.'
    redirect_to users_url
  end

  def feed
    Micropost.where('user_id = ?', id)
    # @feed_items = current_user.feed.paginate(page: params[:page])
  end

  private

  def admin_user
    redirect_to(root_url) unless current_user.admin?
  end

  def user_params
    params.require(:user).permit(:name, :email, :password,
                                 :password_confirmation, :id)
  end
end
