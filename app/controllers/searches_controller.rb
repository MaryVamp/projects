class SearchesController < ApplicationController

  def new
    @search = Search.new
    @titles = Post.pluck.uniq(:title)
    @user_names = User.pluck.uniq(:name)

  end

  def create
    @search = Search.create(search_params)
    redirect_to @search
  end

  def show
    @search = Search.find(params[:id])
  end

  def search
    @articles = params[:q] ? Article.search params[:q] : []
  end

  private

  def search_params
    params.require(:search).permit(:keywords, :title, :name)
  end

end
