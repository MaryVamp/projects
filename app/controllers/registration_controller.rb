class RegistrationController < Devise::RegistrationController
  set_flash_message(:notice, :signed_in) if is_flashing_format?

  def edit
    current_user = User.find(params[:id])
  end

  private

  def sign_up_params
    params.require(:user).permit(:email, :password, :password_confirmation)
  end

  def new_user_params
    params.require(:user).permit(:email, :password, :password_confirmation)
  end

  def account_update_params
    params.require(:user).permit(:email, :password, :password_confirmation)
  end
end
