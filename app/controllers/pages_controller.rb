class PagesController < ApplicationController
  before_action :authenticate_user!, exept: [:index, :show]

  def about
  end

  def index
  end
end
