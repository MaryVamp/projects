class PostsController < ApplicationController
  before_action :authenticate_user!, exept: [:index, :show]
  before_action current_post
  def index
    @posts = Post.search(params[:search])
  end

  def new
    @user_name = current_user.name
    @post = Post.new
  end

  def create
    @post = Post.new(post_params)
    @post.user = current_user

    if @post.save
      PostMailer.post_created(user).deliver_later
      flash[:notice] = 'Post was created successfully'
      redirect_to posts_path
    else
      render :new
    end
  end

  def show
    @post = Post.find(params[:id])
  end

  def edit
    @post = Post.find(params[:id])
  end

  def update
    @post = Post.find(params[:id])

    if @post.save
      redirect_to @post
    else
      render 'new'
    end
  end

  def destroy
    Post.find(params[:id]).destroy
    redirect_to root_path
  end

  private

  def post_params
    params.require(:post).permit(:title, :body, :attached_image, :name, :id, :user_name)
  end

  def current_post
    @post = Post.find(params[:id])
  end
end
