class CommentsController < ApplicationController
  before_action :comments_params

  def create
    @post = Post.find(params[:post_id])
    @comment = @post.comments.create
    CommentMailer.comment_created(current_user, @post.user, @comment.body).deliver
    redirect_to post_path(@post)
  end

  def destroy
    @post = Post.find(params[:post_id])
    @comment = @post.comments.find(params[:id])
    @comment.destroy

    redirect_to post_path(@post)
  end

  private

  def correct_user
    @micropost = current_user.microposts.find_by(id: params[:id])
    redirect_to root_url unless @micropost
  end

  def comments_params
    params[:comment].permit(:name, :body)
  end
end
