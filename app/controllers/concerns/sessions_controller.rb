class SessionsController < Devise::SessionsController
  # set_flash_message(:notice, :signed_in) if is_flashing_format?

  def create
    self.resource = warden.authenticate!(auth_options)
    flash[:notice] = "Welcome back, #{current_user.email}" if is_flashing_format?
    sign_in(resource_name, resource)
    yield resource if block_given?
    respond_with resource, location: after_sign_in_path_for(resource)
  end

  private

  def sign_up_params
    params.require(:user).permit(:email, :password, :password_confirmation)
  end

  def account_update_params
    params.require(:user).permit(:email, :password, :password_confirmation)
  end
end
