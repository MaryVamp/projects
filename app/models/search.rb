class Search < ActiveRecord::Base
  def search_posts
    posts = Post.all

    posts = posts.where(['title LIKE ?', "%#{keywords}%"]) if keywords.present?
    posts = posts.where(['body LIKE ?', "%#{keywords}%"]) if keywords.present?
    posts = posts.where(['name LIKE ?', name]) if name.present?

    posts
  end
end
