class Micropost < ActiveRecord::Base
  validates :user_id, presence: true
  belongs_to :user
  validates :content, presence: true, length: { maximum: 240 }
end
