class User < ActiveRecord::Base
  before_save { self.email = email.downcase }
  devise :database_authenticatable, :registerable,
         :rememberable, :trackable, :validatable, :confirmable
  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
  validates :email, presence: true, format: { with: VALID_EMAIL_REGEX },
                    uniqueness: { case_sensitive: false }
  validates :password, presence: true
  has_many :posts
  has_many :microposts, dependent: :destroy
  after_create :send_notification

  acts_as_messageable

  def send_notification
    AdminMailer.new_user(self).deliver_now
  end

  def mailboxer_name
    name
  end

  def mailboxer_email(_object)
    email
  end
end
