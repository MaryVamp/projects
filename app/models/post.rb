require 'elasticsearch/model'
class Post < ActiveRecord::Base
  include Elasticsearch::Model
  include Elasticsearch::Model::Callbacks

  has_many :comments, dependent: :destroy
  belongs_to :user
  validates :title, presence: true, length: { minimum: 5 }
  validates :body, presence: true

  has_attached_file :attached_image, styles: { medium: '600x600>', thumb: '200x200>' }, default_url: '/images/:style/missing.png'
  validates_attachment_content_type :attached_image, content_type: /\Aimage\/.*\Z/

  def self.search(search)
    if search
      where(['title LIKE ?', "%#{search}%"])
    else
      all
    end
  end

  settings index: { number_of_shards: 1 } do
    mappings dynamic: 'false' do
      indexes :title, analyzer: 'russian', index_options: 'offsets'
      indexes :text, analyzer: 'russian'
    end
  end
end
# Post.import
