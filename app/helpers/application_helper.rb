module ApplicationHelper
  def flash_class(level)
    case level.to_s
    when :success then
      'ui success message'
    when :error then
      'ui negative message'
    when :alert then
      'ui warning message'
    when :notice then
      'ui info message'
      # else
      #   flash_type.to_s
    end
  end

  def page_header(text)
    content_for(:page_header) { text.to_s }
  end
end
