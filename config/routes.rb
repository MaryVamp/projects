Rails.application.routes.draw do
  mount RailsAdmin::Engine => '/admin', as: 'rails_admin'
  scope "(:locale)", locale: /ru|en/ do
    get '/:locale' => 'posts#index'
    devise_for :users, controllers: { registration: "registrations" }

      get 'users/sign_out', :method => :delete
      # get '/:id/users/edit' => 'devise/registrations#edit'
      get '/users/password/edit' => 'devise/password#edit'
      get '/:id/users/edit', to: 'users#edit'
    resources :posts do
      resources :comments
    end
    resources :users do

    end
      root 'pages#about'
      get  '/:user_id/pages/index' => 'pages#index', as: 'index'

      resources :conversations, only: [:index, :show, :destroy] do
        member do
          post :reply
          post :restore
          post :mark_as_read
        end
        collection do
          delete :empty_trash
        end
      end
      resources :messages, only: [:new, :create]
        post '/messages/new', to: 'messages#new'
    resources :searches
    get '/change_locale/:locale', to: 'application#change_locale', as: :change_locale
      get 'searches', to: 'searches#search', as: :custom_search
  end
end
